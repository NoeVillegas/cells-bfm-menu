# &lt;cells-bfm-menu&gt;

Your component description.

![Polymer 2.x](https://img.shields.io/badge/Polymer-2.x-green.svg)
Example:
```html
<cells-bfm-menu></cells-bfm-menu>
```

## Styling

The following custom properties and mixins are available for styling:

### Custom Properties
| Custom Property                  | Selector           | CSS Property     | Value                                                   |
| -------------------------------- | ------------------ | ---------------- | ------------------------------------------------------- |
| --bbva-navy                      | :host              | background-color | `No fallback value`                                     |
| --cells-fontDefault              | :host              | font-family      | sans-serif                                              |
| --bbva-navy                      | .menu-container    | background-color | `No fallback value`                                     |
| --cells-bfm-menu-link-item-color | .link-item         | color            | ![#fff](https://placehold.it/15/fff/000000?text=+) #fff |
| --bbva-yellow                    | .session-container | color            | `No fallback value`                                     |
### @apply
| Mixins                                  | Selector                 | Value |
| --------------------------------------- | ------------------------ | ----- |
| --cells-bfm-menu                        | :host                    | {}    |
| --cells-bfm-menu-logo                   | .logo                    | {}    |
| --cells-bfm-menu-container              | .menu-container          | {}    |
| --cells-bfm-menu-link-item              | .link-item               | {}    |
| --cells-bfm-menu-link-item-active-after | .link-item.active::after | {}    |
| --cells-bfm-menu-btn-icon               | .btn-icon                | {}    |
| --cells-bfm-menu-session-container      | .session-container       | {}    |
| --cells-bfm-menu-extra                  | .extra-menu              | {}    |
| --cells-bfm-menu-section                | .menu-section            | {}    |
| --cells-bfm-menu-session-section        | .session-section         | {}    |
| --cells-bfm-menu-sub-menu-container     | .sub-menu-container      | {}    |
| --cells-bfm-menu-sub-menu-item          | .sub-menu-item           | {}    |
| --cells-bfm-menu-extra-menu-item        | .extra-menu-item         | {}    |
