{
  const {
    html,
  } = Polymer;
  /**
    `<cells-bfm-menu>` Description.

    Example:

    ```html
    < ></cells-bfm-menu>
    ```

    ## Styling
    The following custom properties and mixins are available for styling:

    ### Custom Properties
    | Custom Property     | Selector | CSS Property | Value       |
    | ------------------- | -------- | ------------ | ----------- |
    | --cells-fontDefault | :host    | font-family  |  sans-serif |
    ### @apply
    | Mixins                                  | Selector                 | Value |
    | --------------------------------------- | ------------------------ | ----- |
    | --cells-bfm-menu                        | :host                    | {}    |
    | --cells-bfm-menu-logo                   | .logo                    | {}    |
    | --cells-bfm-menu-container              | .menu-container          | {}    |
    | --cells-bfm-menu-link-item              | .link-item               | {}    |
    | --cells-bfm-menu-link-item-active-after | .link-item.active::after | {}    |
    | --cells-bfm-menu-btn-icon               | .btn-icon                | {}    |
    | --cells-bfm-menu-session-container      | .session-container       | {}    |
    | --cells-bfm-menu-extra                  | .extra-menu              | {}    |
    | --cells-bfm-menu-section                | .menu-section            | {}    |
    | --cells-bfm-menu-session-section        | .session-section         | {}    |
    | --cells-bfm-menu-sub-menu-container     | .sub-menu-container      | {}    |
    | --cells-bfm-menu-sub-menu-item          | .sub-menu-item           | {}    |
    | --cells-bfm-menu-extra-menu-item        | .extra-menu-item         | {}    |
    * @customElement
    * @polymer
    * @extends {Polymer.Element}
    * @demo demo/index.html
  */
  class CellsBfmMenu extends Polymer.Element {

    static get is() {
      return 'cells-bfm-menu';
    }

    static get properties() {
      return {
        /**
         * Property to set source of image logo
         */
        logo: {
          type: Object,
          value: {
            src: '',
            alt: ''
          }
        },
        /**
         * Array property to create menu items, object on array is like:
         * Example:
         *  {
         *    text:'Global Position',
         *    page:'go-to-pg'
         *  }
         */
        menu: {
          type: Array,
          value: function() {
            return [
              {
                text: '',
                event: ''
              }
            ];
          }
        },
        /**
         * Array property to create menu items, object on array is like:
         * Example:
         *  {
         *    text:'Configuración',
         *    event:'go-to-config-page',
         *    icon: '',
         *    items: [
         *      {
         *        text: '',
         *        event: '',
         *        icon: ''
         *      }
         *    ]
         *  }
         */
        menuExtra: {
          type: Array,
          value: () => [
            {
              text: '',
              event: '',
              showExtra: false,
              items: []
            }
          ]
        },
        /**
         * @description Object to configure header session info like user name and logout link
         * @example {
                      name: 'Noe Villegas',
                      icon: 'coronita:getout',
                      textLink: 'Exit'
                    }
         * @public
         * @type Object
         */
        session: {
          type: Object,
          value: {
            name: '',
            icon: '',
            textLink: ''
          }
        },
        /**
         * @description Determines wich link menu is active
         * @example 'PERFIL'
         * @type String
         * @public
         */
        active: {
          type: String
        }
      };
    }
    /**
     * Define wich menu has active class
     * @param {String} textMenu
     */
    _getClass(textMenu, active) {
      return (textMenu === active) ? 'active' : '';
    }
    /**
     * Dispatch custom event, this method is launched when menu link is clicked and
     * get event name from menu property
     * @param {event} e
     */
    _handleClick(e) {
      this.dispatchEvent(new CustomEvent(e.model.__data.item.event));
    }
    /**
     * Dispatch custom event, this method is launched when session link is clicked and
     * send session-click-link
     */
    _sessionEvent() {
      this.dispatchEvent(new CustomEvent('session-click-link'));
    }
    /**
    * Dispatch custom event, this method is launched when logo is clicked and
    * send event logo-click
    */
    _logoClick() {
      this.dispatchEvent(new CustomEvent('logo-click'));
    }

    /**
     * Function to handle events from extra menu items
     * @param {*} event
     */
    _handleExtraMenu(event) {
      this.dispatchEvent(new CustomEvent(event.model.__data.extra.event));
      var menu = event.model.__data;
      var isVisibleSub = (menu.extra.showExtra) ? !menu.extra.showExtra : true;
      this.set(`menuExtra.${menu.index}.showExtra`, isVisibleSub);
    }

  }

  customElements.define(CellsBfmMenu.is, CellsBfmMenu);
}